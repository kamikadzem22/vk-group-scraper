import os
from time import sleep

import requests
import json
import argparse


class VkGroupApi:
    def __init__(self, service_key, api_version='5.52'):
        self.api_key = service_key
        self.api_version = api_version
        self.url = "https://api.vk.com/method/wall.get?v={api_version}&access_token={key}&domain={domain}"
        self.current_url = None
        self.posts_count = None

    def init_group(self, domain):
        self.current_url = self.url.format(
            api_version=self.api_version,
            key=self.api_key,
            domain=domain
        )
        try:
            response = json.loads(
                requests.get(
                    url=self.current_url + "&count=1&offset=0"
                ).text
            )
            self.posts_count = response['response']['count']
        except KeyError:
            raise Exception("Api Error! {}".format(response['error']['error_msg']))

    def get_posts(self, count=100, offset=0):
        if self.current_url is None:
            raise Exception('No group is selected')

        if count > self.posts_count - offset:
            raise Exception('Not enough posts to scrape. Total posts: {max}'.format(max=self.posts_count))

        posts = []
        offset = 0
        for i in range((count - offset + 99) // 100):
            data = requests.get(
                self.current_url + "&offset={offset}&count={count}".format(
                    offset=offset,
                    count=min(count, 100)
                )).text
            posts += [post['text'] for post in json.loads(data)['response']['items']]
            offset += 100
            sleep(1)
        return posts

    def get_all_posts(self):
        return self.get_posts(count=self.posts_count)


def main(args):
    api_key = os.environ.get("VK_SERVICE_KEY")
    if args.key is not None:
        api_key = args.key[0]
    vk = VkGroupApi(api_key)
    vk.init_group(domain=args.domain[0])
    if args.max:
        posts = vk.get_all_posts()
    elif args.count is not None:
        posts = vk.get_posts(count=args.count[0])
    else:
        posts = vk.get_posts()
    print(*posts)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Simple script to scrape text posts from VK.com groups by domain and service key"
    )
    parser.add_argument("-d", "--domain", nargs=1, type=str, required=True, help="Group domain (e.g. vk.com/baneks)")
    parser.add_argument("-c", "--count", type=int, nargs=1, help="Number of posts to scrape. Default is 100")
    parser.add_argument("-m", "--max", help="Scrape all posts. --count will be ignored", action='store_true')
    parser.add_argument(
        "--key",
        help="Service key. Not required as script uses environmental variable VK_SERVICE_KEY by default",
        nargs=1,
    )
    args = parser.parse_args()
    main(args)
