# Simple vk.com posts scraper

## Usage
Your service key must be supplied using environmental variable ```VK_SERVICE_KEY``` or parameter ```--key```
```
usage: scrape-group.py [-h] -d DOMAIN [-c COUNT] [-m] [--key KEY]

Simple script to scrape text posts from VK.com groups by domain and service
key

optional arguments:
  -h, --help            show this help message and exit
  -d DOMAIN, --domain DOMAIN
                        Group domain (e.g. vk.com/baneks)
  -c COUNT, --count COUNT
                        Number of posts to scrape. Default is 100
  -m, --max             Scrape all posts. --count will be ignored
  --key KEY             Service key. Not required as script uses environmental
                        variable VK_SERVICE_KEY by default
```